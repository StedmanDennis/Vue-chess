Vue.component('chess-piece',{
	props: ['gameTile','gameInstance'],
	template: 	`
				<pawn v-if='gameTile.occupiedBy.type == "pawn"' v-bind:gameTile='gameTile' v-bind:gameInstance='gameInstance' v-bind:piece='this.gameTile.occupiedBy'></pawn>
				<rook v-else-if='gameTile.occupiedBy.type == "rook"' v-bind:gameTile='gameTile' v-bind:gameInstance='gameInstance' v-bind:piece='this.gameTile.occupiedBy'></rook>
				<knight v-else-if='gameTile.occupiedBy.type == "knight"' v-bind:gameTile='gameTile' v-bind:gameInstance='gameInstance' v-bind:piece='this.gameTile.occupiedBy'></knight>
				<bishop v-else-if='gameTile.occupiedBy.type == "bishop"' v-bind:gameTile='gameTile' v-bind:gameInstance='gameInstance' v-bind:piece='this.gameTile.occupiedBy'></bishop>
				<queen v-else-if='gameTile.occupiedBy.type == "queen"' v-bind:gameTile='gameTile' v-bind:gameInstance='gameInstance' v-bind:piece='this.gameTile.occupiedBy'></queen>
				<king v-else-if='gameTile.occupiedBy.type == "king"' v-bind:gameTile='gameTile' v-bind:gameInstance='gameInstance' v-bind:piece='this.gameTile.occupiedBy'></king>
				`,
})

Vue.component('pawn', {
	props: ['gameTile','gameInstance','piece'],
	template: 	`
				<img src='./images/pieces/white/ChessWhitePawn.png' v-if='gameTile.occupiedBy.pieceColor == "white"' v-on:click='updateSelected'> 
				<img src='./images/pieces/black/ChessBlackPawn.png' v-else-if='gameTile.occupiedBy.pieceColor == "black"' v-on:click='updateSelected'> 
				`,
	methods: {
		calculateLegalMoves: function(){
			var legalMoves = [];
			var legalCaptures = [];
			thisPiece = this.piece;
			piecePosInInstance = (8 * thisPiece.rowPos + thisPiece.colPos);
			/*
			if (!thisPiece.hasMoved){
				if (thisPiece.playerId == 0){
					if (this.gameInstance[piecePosInInstance + (8 * 1)].occupiedBy == null){
						legalMoves.push(piecePosInInstance + (8 * 1));
						if (this.gameInstance[piecePosInInstance + (8 * 2)].occupiedBy == null){
							legalMoves.push(piecePosInInstance + (8 * 2));
						}
					}
					//capture logic
					if (this.gameInstance[piecePosInInstance + ((8 * 1) - 1)].occupiedBy != null && this.gameInstance[piecePosInInstance + (8 * 1 - 1)].occupiedBy.playerId == 1){
						console.log("capturable");
					}
					if (this.gameInstance[piecePosInInstance + ((8 * 1) + 1)].occupiedBy != null && this.gameInstance[piecePosInInstance + (8 * 1 + 1)].occupiedBy.playerId == 1){
						console.log("capturable");
					}
					//
				}else if (thisPiece.playerId == 1){
					if (this.gameInstance[piecePosInInstance - (8 * 1)].occupiedBy == null){
						legalMoves.push(piecePosInInstance - (8 * 1));
						if (this.gameInstance[piecePosInInstance - (8 * 2)].occupiedBy == null){
							legalMoves.push(piecePosInInstance - (8 * 2));
						}
					}
				}
			}else{
				if (thisPiece.playerId == 0){
					if (this.gameInstance[piecePosInInstance + (8 * 1)].occupiedBy == null){
						legalMoves.push(piecePosInInstance + (8 * 1));
					}
				}else if (thisPiece.playerId == 1){
					if (this.gameInstance[piecePosInInstance - (8 * 1)].occupiedBy == null){
						legalMoves.push(piecePosInInstance - (8 * 1));
					}
				}
			}
			*/

			if (thisPiece.playerId == 0){
				if (!thisPiece.hasMoved){
					if (this.gameInstance[piecePosInInstance + (8 * 1)].occupiedBy == null && piecePosInInstance + (8 * 1) <= 63){
						legalMoves.push(piecePosInInstance + (8 * 1));
						if (this.gameInstance[piecePosInInstance + (8 * 2)].occupiedBy == null && piecePosInInstance + (8 * 2) <= 63){
							legalMoves.push(piecePosInInstance + (8 * 2));
						}
					}
				}else{
					if (this.gameInstance[piecePosInInstance + (8 * 1)].occupiedBy == null && piecePosInInstance + (8 * 1) <= 63){
						legalMoves.push(piecePosInInstance + (8 * 1));
					}
				}
				//capture logic
				if (piecePosInInstance - ((8 * 1) + 1) <= 63 && this.gameInstance[piecePosInInstance + ((8 * 1) - 1)].occupiedBy != null && (piecePosInInstance + (8 * 1 - 1)) % 8 != 7 && this.gameInstance[piecePosInInstance + (8 * 1 - 1)].occupiedBy.playerId == 1){
					console.log("capturable");
					legalCaptures.push(piecePosInInstance + ((8 * 1) - 1));
				}
				if (piecePosInInstance - ((8 * 1) + 1) <= 63 && this.gameInstance[piecePosInInstance + ((8 * 1) + 1)].occupiedBy != null && (piecePosInInstance + (8 * 1 + 1)) % 8 != 0 && this.gameInstance[piecePosInInstance + (8 * 1 + 1)].occupiedBy.playerId == 1){
					console.log("capturable");
					legalCaptures.push(piecePosInInstance + ((8 * 1) + 1));
				}
				//
			}else if (thisPiece.playerId == 1){
				if (!thisPiece.hasMoved){
					if (this.gameInstance[piecePosInInstance - (8 * 1)].occupiedBy == null && piecePosInInstance - (8 * 1) >= 0){
						legalMoves.push(piecePosInInstance - (8 * 1));
						if (this.gameInstance[piecePosInInstance - (8 * 2)].occupiedBy == null && piecePosInInstance - (8 * 1) >= 0){
							legalMoves.push(piecePosInInstance - (8 * 2));
						}
					}
				}else{
					if (this.gameInstance[piecePosInInstance - (8 * 1)].occupiedBy == null && piecePosInInstance - (8 * 1) >= 0){
						legalMoves.push(piecePosInInstance - (8 * 1));
					}
				}
				//capture logic
				if (piecePosInInstance - ((8 * 1) - 1) >= 0 && this.gameInstance[piecePosInInstance - ((8 * 1) - 1)].occupiedBy != null && (piecePosInInstance - (8 * 1 - 1)) % 8 != 0 && this.gameInstance[piecePosInInstance - (8 * 1 - 1)].occupiedBy.playerId == 0){
					console.log("capturable");
					legalCaptures.push(piecePosInInstance - ((8 * 1) - 1));
				}
				if (piecePosInInstance - ((8 * 1) + 1) >= 0 && this.gameInstance[piecePosInInstance - ((8 * 1) + 1)].occupiedBy != null && (piecePosInInstance - (8 * 1 + 1)) % 8 != 7 && this.gameInstance[piecePosInInstance - (8 * 1 + 1)].occupiedBy.playerId == 0){
					console.log("capturable");
					legalCaptures.push(piecePosInInstance - ((8 * 1) + 1));
				}
				//
			}
			thisPiece.legalMoves = legalMoves;
			thisPiece.legalCaptures = legalCaptures;
		},
		updateSelected(){
			console.log(this.gameTile.capturable)
			if (this.gameTile.capturable){
				console.log('gotem'); //allows piece capturing
			}else{
				this.$parent.$parent.$parent.$emit('pieceSelected',this);
			}
			//this.$parent.$parent.$parent.$emit('pieceSelected',this);
		},
		highlightLegalMoves: function(){
			//this.updateSelected();
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			this.calculateLegalMoves();
			if (this.$parent.$parent.$parent.$parent.selectedPiece == this){
				console.log(thisPiece.legalMoves);
				for (var move = 0; move < thisPiece.legalMoves.length; move++){
					thisGame[thisPiece.legalMoves[move]].selectable = true;
				}
				console.log(thisPiece.legalCaptures);
				for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
					thisGame[thisPiece.legalCaptures[cap]].capturable = true;
				}
			}
		},
		dehighlightLegalMoves: function(){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			for (var move = 0; move < thisPiece.legalMoves.length; move++){
				thisGame[thisPiece.legalMoves[move]].selectable = false;
			}
			for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
				thisGame[thisPiece.legalCaptures[cap]].capturable = false;
			}
		},
		commitMove: function(row, col){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			posBeforeMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posBeforeMove].occupiedBy = null;
			thisPiece.rowPos = row;
			thisPiece.colPos = col;
			posAfterMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posAfterMove].occupiedBy = thisPiece;
			thisPiece.hasMoved = true;
			this.dehighlightLegalMoves();
			eventBus.$emit('committedMove', {piece:thisPiece, before:posBeforeMove, after:posAfterMove});
		}
	}
})

Vue.component('rook', {
	props: ['gameTile','gameInstance','piece'],
	template: 	`
				<img src='./images/pieces/white/ChessWhiteRook.png' v-if='gameTile.occupiedBy.pieceColor == "white"' v-on:click='updateSelected'>
				<img src='./images/pieces/black/ChessBlackRook.png' v-else-if='gameTile.occupiedBy.pieceColor == "black"' v-on:click='updateSelected'> 
				`,
	methods: {
		calculateLegalMoves: function(){
			var legalMoves = [];
			var legalCaptures = [];
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			piecePosInInstance = (8 * thisPiece.rowPos + thisPiece.colPos);
			//Upward
			for (var up = piecePosInInstance - 8; up >= 0; up-=8){
				if (thisGame[up].occupiedBy == null){
					legalMoves.push(up);
				}else if (thisGame[up].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(up);
					break;
				}else{
					break;
				}
			}
			//
			//Downward
			for (var down = piecePosInInstance + 8; down <= 63; down+=8){
				if (thisGame[down].occupiedBy == null){
					legalMoves.push(down);
				}else if (thisGame[down].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(down);
					break;
				}else{
					break;
				}
			}
			//
			//Right
			for (var right = piecePosInInstance + 1; right % 8 != 0 && right <= 63; right++){
				if (thisGame[right].occupiedBy == null){
					legalMoves.push(right);
				}else if (thisGame[right].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(right);
					break;
				}else{
					break;
				}
			}
			//
			//Left
			for (var left = piecePosInInstance - 1; left % 8 != 7 && left >= 0; left--){
				if (thisGame[left].occupiedBy == null){
					legalMoves.push(left);
				}else if (thisGame[left].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(left);
					break;
				}else{
					break;
				}
			}
			//
			thisPiece.legalMoves = legalMoves;
			thisPiece.legalCaptures = legalCaptures;
		},
		updateSelected(){
			console.log(this.gameTile.capturable)
			if (this.gameTile.capturable){
				console.log('gotem'); //allows piece capturing
			}else{
				this.$parent.$parent.$parent.$emit('pieceSelected',this);
			}
			//this.$parent.$parent.$parent.$emit('pieceSelected',this);
		},
		highlightLegalMoves: function(){
			//this.updateSelected();
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			this.calculateLegalMoves();
			if (this.$parent.$parent.$parent.$parent.selectedPiece == this){
				console.log(thisPiece.legalMoves);
				for (var move = 0; move < thisPiece.legalMoves.length; move++){
					thisGame[thisPiece.legalMoves[move]].selectable = true;
				}
				console.log(thisPiece.legalCaptures);
				for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
					thisGame[thisPiece.legalCaptures[cap]].capturable = true;
				}
			}
		},
		dehighlightLegalMoves: function(){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			for (var move = 0; move < thisPiece.legalMoves.length; move++){
				thisGame[thisPiece.legalMoves[move]].selectable = false;
			}
			for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
				thisGame[thisPiece.legalCaptures[cap]].capturable = false;
			}
		},
		commitMove: function(row, col){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			posBeforeMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posBeforeMove].occupiedBy = null;
			thisPiece.rowPos = row;
			thisPiece.colPos = col;
			posAfterMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posAfterMove].occupiedBy = thisPiece;
			thisPiece.hasMoved = true;
			this.dehighlightLegalMoves();
			eventBus.$emit('committedMove', {piece:thisPiece, before:posBeforeMove, after:posAfterMove});
		}
	}
})

Vue.component('knight', {
	props: ['gameTile','gameInstance','piece'],
	template: 	`
				<img src='./images/pieces/white/ChessWhiteKnight.png' v-if='gameTile.occupiedBy.pieceColor == "white"' v-on:click='updateSelected'>
				<img src='./images/pieces/black/ChessBlackKnight.png' v-else-if='gameTile.occupiedBy.pieceColor == "black"' v-on:click='updateSelected'> 
				`,
	methods: {
		calculateLegalMoves: function(){
			var legalMoves = [];
			var legalCaptures = [];
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			piecePosInInstance = (8 * thisPiece.rowPos + thisPiece.colPos);
			var up = piecePosInInstance-16;
			var upLeft = up-1;
			var upRight = up+1;
			var down = piecePosInInstance+16;
			var downLeft = down-1;
			var downRight = down+1;
			var left = piecePosInInstance-2;
			var leftUp = left-8;
			var leftDown = left+8;
			var right = piecePosInInstance+2;
			var rightUp = right-8;
			var rightDown = right+8;
			//Upward (left/right)
			if (upLeft >= 0 && upLeft % 8 < 7){ //left
				if (thisGame[upLeft].occupiedBy == null){
					legalMoves.push(upLeft);
				}else if (thisGame[upLeft].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(upLeft);
				}
			}
			if (upRight >= 0 && upRight % 8 > 0){ //right
				if (thisGame[upRight].occupiedBy == null){
					legalMoves.push(upRight);
				}else if (thisGame[upRight].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(upRight);
				}
			}
			//
			//Downward (left/right)
			if (downLeft <= 63 && downLeft % 8 < 7){ //left
				if (thisGame[downLeft].occupiedBy == null){
					legalMoves.push(downLeft);
				}else if (thisGame[downLeft].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(downLeft);
				}
			}
			if (downRight <= 63 && downRight % 8 > 0){ //right
				if (thisGame[downRight].occupiedBy == null){
					legalMoves.push(downRight);
				}else if (thisGame[downRight].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(downRight);
				}
			}
			//
			//Right (up/down)
			if (rightUp >= 0 && rightUp % 8 > 1){
				if (thisGame[rightUp].occupiedBy == null){
					legalMoves.push(rightUp);
				}else if (thisGame[rightUp].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(rightUp);
				}
			}
			if (rightDown <= 63 && rightDown % 8 > 1){
				if (thisGame[rightDown].occupiedBy == null){
					legalMoves.push(rightDown);
				}else if (thisGame[rightDown].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(rightDown);
				}
			}
			//
			//Left (up/down)
			if (leftUp >= 0 && leftUp % 8 < 6){
				if (thisGame[leftUp].occupiedBy == null){
					legalMoves.push(leftUp);
				}else if (thisGame[leftUp].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(leftUp);
				}
			}
			if (leftDown <= 63 && leftDown % 8 < 6){
				if (thisGame[leftDown].occupiedBy == null){
					legalMoves.push(leftDown);
				}else if (thisGame[leftDown].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(leftDown);
				}
			}
			//
			thisPiece.legalMoves = legalMoves;
			thisPiece.legalCaptures = legalCaptures;
		},
		updateSelected(){
			console.log(this.gameTile.capturable)
			if (this.gameTile.capturable){
				console.log('gotem'); //allows piece capturing
			}else{
				this.$parent.$parent.$parent.$emit('pieceSelected',this);
			}
			//this.$parent.$parent.$parent.$emit('pieceSelected',this);
		},
		highlightLegalMoves: function(){
			//this.updateSelected();
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			this.calculateLegalMoves();
			if (this.$parent.$parent.$parent.$parent.selectedPiece == this){
				console.log(thisPiece.legalMoves);
				for (var move = 0; move < thisPiece.legalMoves.length; move++){
					thisGame[thisPiece.legalMoves[move]].selectable = true;
				}
				console.log(thisPiece.legalCaptures);
				for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
					thisGame[thisPiece.legalCaptures[cap]].capturable = true;
				}
			}
		},
		dehighlightLegalMoves: function(){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			for (var move = 0; move < thisPiece.legalMoves.length; move++){
				thisGame[thisPiece.legalMoves[move]].selectable = false;
			}
			for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
				thisGame[thisPiece.legalCaptures[cap]].capturable = false;
			}
		},
		commitMove: function(row, col){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			posBeforeMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posBeforeMove].occupiedBy = null;
			thisPiece.rowPos = row;
			thisPiece.colPos = col;
			posAfterMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posAfterMove].occupiedBy = thisPiece;
			thisPiece.hasMoved = true;
			this.dehighlightLegalMoves();
			eventBus.$emit('committedMove', {piece:thisPiece, before:posBeforeMove, after:posAfterMove});
		}
	}
})

Vue.component('bishop', {
	props: ['gameTile','gameInstance','piece'],
	template: 	`
				<img src='./images/pieces/white/ChessWhiteBishop.png' v-if='gameTile.occupiedBy.pieceColor == "white"' v-on:click='updateSelected'>
				<img src='./images/pieces/black/ChessBlackBishop.png' v-else-if='gameTile.occupiedBy.pieceColor == "black"' v-on:click='updateSelected'> 
				`,
	methods:{
		calculateLegalMoves: function(){
			var legalMoves = [];
			var legalCaptures = [];
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			piecePosInInstance = (8 * thisPiece.rowPos + thisPiece.colPos);
			//Up-left
			for (var upLeft = piecePosInInstance - 9; upLeft >= 0 && upLeft % 8 < 7; upLeft-=9){
				if (thisGame[upLeft].occupiedBy == null){
					legalMoves.push(upLeft);
				}else if (thisGame[upLeft].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(upLeft);
					break;
				}else{
					break;
				}
			}
			//
			//Up-right
			for (var upRight = piecePosInInstance - 7; upRight >= 0 && upRight % 8 > 0; upRight-=7){
				if (thisGame[upRight].occupiedBy == null){
					legalMoves.push(upRight);
				}else if (thisGame[upRight].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(upRight);
					break;
				}else{
					break;
				}
			}
			//
			//Down-left
			for (var downLeft = piecePosInInstance + 7; downLeft <= 63 && downLeft % 8 < 7; downLeft+=7){
				if (thisGame[downLeft].occupiedBy == null){
					legalMoves.push(downLeft);
				}else if (thisGame[downLeft].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(downLeft);
					break;
				}else{
					break;
				}
			}
			//
			//Down-right
			for (var downRight = piecePosInInstance + 9; downRight <= 63 && downRight % 8 > 0; downRight+=9){
				if (thisGame[downRight].occupiedBy == null){
					legalMoves.push(downRight);
				}else if (thisGame[downRight].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(downRight);
					break;
				}else{
					break;
				}
			}
			//
			thisPiece.legalMoves = legalMoves;
			thisPiece.legalCaptures = legalCaptures;
		},
		updateSelected(){
			console.log(this.gameTile.capturable)
			if (this.gameTile.capturable){
				console.log('gotem'); //allows piece capturing
			}else{
				this.$parent.$parent.$parent.$emit('pieceSelected',this);
			}
			//this.$parent.$parent.$parent.$emit('pieceSelected',this);
		},
		highlightLegalMoves: function(){
			//this.updateSelected();
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			this.calculateLegalMoves();
			if (this.$parent.$parent.$parent.$parent.selectedPiece == this){
				console.log(thisPiece.legalMoves);
				for (var move = 0; move < thisPiece.legalMoves.length; move++){
					thisGame[thisPiece.legalMoves[move]].selectable = true;
				}
				console.log(thisPiece.legalCaptures);
				for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
					thisGame[thisPiece.legalCaptures[cap]].capturable = true;
				}
			}
		},
		dehighlightLegalMoves: function(){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			for (var move = 0; move < thisPiece.legalMoves.length; move++){
				thisGame[thisPiece.legalMoves[move]].selectable = false;
			}
			for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
				thisGame[thisPiece.legalCaptures[cap]].capturable = false;
			}
		},
		commitMove: function(row, col){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			posBeforeMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posBeforeMove].occupiedBy = null;
			thisPiece.rowPos = row;
			thisPiece.colPos = col;
			posAfterMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posAfterMove].occupiedBy = thisPiece;
			thisPiece.hasMoved = true;
			this.dehighlightLegalMoves();
			eventBus.$emit('committedMove', {piece:thisPiece, before:posBeforeMove, after:posAfterMove});
		}
	}
})

Vue.component('queen', {
	props: ['gameTile','gameInstance','piece'],
	template: 	`
				<img src='./images/pieces/white/ChessWhiteQueen.png' v-if='gameTile.occupiedBy.pieceColor == "white"' v-on:click='updateSelected'>
				<img src='./images/pieces/black/ChessBlackQueen.png' v-else-if='gameTile.occupiedBy.pieceColor == "black"' v-on:click='updateSelected'> 
				`,
	methods: {
		calculateLegalMoves: function(){
			var legalMoves = [];
			var legalCaptures = [];
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			piecePosInInstance = (8 * thisPiece.rowPos + thisPiece.colPos);
			//Upward
			for (var up = piecePosInInstance - 8; up >= 0; up-=8){
				if (thisGame[up].occupiedBy == null){
					legalMoves.push(up);
				}else if (thisGame[up].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(up);
					break;
				}else{
					break;
				}
			}
			//
			//Downward
			for (var down = piecePosInInstance + 8; down <= 63; down+=8){
				if (thisGame[down].occupiedBy == null){
					legalMoves.push(down);
				}else if (thisGame[down].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(down);
					break;
				}else{
					break;
				}
			}
			//
			//Right
			for (var right = piecePosInInstance + 1; right % 8 != 0 && right <= 63; right++){
				if (thisGame[right].occupiedBy == null){
					legalMoves.push(right);
				}else if (thisGame[right].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(right);
					break;
				}else{
					break;
				}
			}
			//
			//Left
			for (var left = piecePosInInstance - 1; left % 8 != 7 && left >= 0; left--){
				if (thisGame[left].occupiedBy == null){
					legalMoves.push(left);
				}else if (thisGame[left].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(left);
					break;
				}else{
					break;
				}
			}
			//
			//Up-left
			for (var upLeft = piecePosInInstance - 9; upLeft >= 0 && upLeft % 8 < 7; upLeft-=9){
				if (thisGame[upLeft].occupiedBy == null){
					legalMoves.push(upLeft);
				}else if (thisGame[upLeft].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(upLeft);
					break;
				}else{
					break;
				}
			}
			//
			//Up-right
			for (var upRight = piecePosInInstance - 7; upRight >= 0 && upRight % 8 > 0; upRight-=7){
				if (thisGame[upRight].occupiedBy == null){
					legalMoves.push(upRight);
				}else if (thisGame[upRight].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(upRight);
					break;
				}else{
					break;
				}
			}
			//
			//Down-left
			for (var downLeft = piecePosInInstance + 7; downLeft <= 63 && downLeft % 8 < 7; downLeft+=7){
				if (thisGame[downLeft].occupiedBy == null){
					legalMoves.push(downLeft);
				}else if (thisGame[downLeft].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(downLeft);
					break;
				}else{
					break;
				}
			}
			//
			//Down-right
			for (var downRight = piecePosInInstance + 9; downRight <= 63 && downRight % 8 > 0; downRight+=9){
				if (thisGame[downRight].occupiedBy == null){
					legalMoves.push(downRight);
				}else if (thisGame[downRight].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(downRight);
					break;
				}else{
					break;
				}
			}
			//
			thisPiece.legalMoves = legalMoves;
			thisPiece.legalCaptures = legalCaptures;
		},
		updateSelected(){
			console.log(this.gameTile.capturable)
			if (this.gameTile.capturable){
				console.log('gotem'); //allows piece capturing
			}else{
				this.$parent.$parent.$parent.$emit('pieceSelected',this);
			}
			//this.$parent.$parent.$parent.$emit('pieceSelected',this);
		},
		highlightLegalMoves: function(){
			//this.updateSelected();
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			this.calculateLegalMoves();
			if (this.$parent.$parent.$parent.$parent.selectedPiece == this){
				console.log(thisPiece.legalMoves);
				for (var move = 0; move < thisPiece.legalMoves.length; move++){
					thisGame[thisPiece.legalMoves[move]].selectable = true;
				}
				console.log(thisPiece.legalCaptures);
				for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
					thisGame[thisPiece.legalCaptures[cap]].capturable = true;
				}
			}
		},
		dehighlightLegalMoves: function(){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			for (var move = 0; move < thisPiece.legalMoves.length; move++){
				thisGame[thisPiece.legalMoves[move]].selectable = false;
			}
			for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
				thisGame[thisPiece.legalCaptures[cap]].capturable = false;
			}
		},
		commitMove: function(row, col){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			posBeforeMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posBeforeMove].occupiedBy = null;
			thisPiece.rowPos = row;
			thisPiece.colPos = col;
			posAfterMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posAfterMove].occupiedBy = thisPiece;
			thisPiece.hasMoved = true;
			this.dehighlightLegalMoves();
			eventBus.$emit('committedMove', {piece:thisPiece, before:posBeforeMove, after:posAfterMove});
		}
	}
})

Vue.component('king', {
	props: ['gameTile','gameInstance','piece'],
	template: 	`
				<img src='./images/pieces/white/ChessWhiteKing.png' v-if='gameTile.occupiedBy.pieceColor == "white"' v-on:click='updateSelected'>
				<img src='./images/pieces/black/ChessBlackKing.png' v-else-if='gameTile.occupiedBy.pieceColor == "black"' v-on:click='updateSelected'> 
				`,
	methods: {
		calculateLegalMoves: function(){
			var legalMoves = [];
			var legalCaptures = [];
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			piecePosInInstance = (8 * thisPiece.rowPos + thisPiece.colPos);
			var up = piecePosInInstance - 8;
			var down = piecePosInInstance + 8;
			var right = piecePosInInstance + 1;
			var left = piecePosInInstance - 1;
			var upLeft = piecePosInInstance - 9;
			var upRight = piecePosInInstance - 7;
			var downLeft = piecePosInInstance + 7;
			var downRight = piecePosInInstance + 9;
			//Upward
			if (up >= 0){
				if (thisGame[up].occupiedBy == null){
					legalMoves.push(up);
				}else if (thisGame[up].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(up);
				}
			}
			//
			//Downward
			if (down <= 63){
				if (thisGame[down].occupiedBy == null){
					legalMoves.push(down);
				}else if (thisGame[down].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(down);
				}
			}
			//
			//Right
			if (right % 8 != 0 && right <= 63){
				if (thisGame[right].occupiedBy == null){
					legalMoves.push(right);
				}else if (thisGame[right].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(right);
				}
			}
			//
			//Left
			if (left % 8 != 7 && left >= 0){
				if (thisGame[left].occupiedBy == null){
					legalMoves.push(left);
				}else if (thisGame[left].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(left);
				}
			}
			//
			//Up-left
			if (upLeft >= 0 && upLeft % 8 < 7){
				if (thisGame[upLeft].occupiedBy == null){
					legalMoves.push(upLeft);
				}else if (thisGame[upLeft].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(upLeft);
				}
			}
			//
			//Up-right
			if (upRight >= 0 && upRight % 8 > 0){
				if (thisGame[upRight].occupiedBy == null){
					legalMoves.push(upRight);
				}else if (thisGame[upRight].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(upRight);
				}
			}
			//
			//Down-left
			if (downLeft <= 63 && downLeft % 8 < 7){
				if (thisGame[downLeft].occupiedBy == null){
					legalMoves.push(downLeft);
				}else if (thisGame[downLeft].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(downLeft);
				}
			}
			//
			//Down-right
			if (downRight <= 63 && downRight % 8 > 0){
				if (thisGame[downRight].occupiedBy == null){
					legalMoves.push(downRight);
				}else if (thisGame[downRight].occupiedBy.playerId != thisPiece.playerId){
					legalCaptures.push(downRight);
				}
			}
			//
			thisPiece.legalMoves = legalMoves;
			thisPiece.legalCaptures = legalCaptures;
		},
		updateSelected(){
			console.log(this.gameTile.capturable)
			if (this.gameTile.capturable){
				console.log('gotem'); //allows piece capturing
			}else{
				this.$parent.$parent.$parent.$emit('pieceSelected',this);
			}
			//this.$parent.$parent.$parent.$emit('pieceSelected',this);
		},
		highlightLegalMoves: function(){
			//this.updateSelected();
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			this.calculateLegalMoves();
			if (this.$parent.$parent.$parent.$parent.selectedPiece == this){
				console.log(thisPiece.legalMoves);
				for (var move = 0; move < thisPiece.legalMoves.length; move++){
					thisGame[thisPiece.legalMoves[move]].selectable = true;
				}
				console.log(thisPiece.legalCaptures);
				for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
					thisGame[thisPiece.legalCaptures[cap]].capturable = true;
				}
			}
		},
		dehighlightLegalMoves: function(){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			for (var move = 0; move < thisPiece.legalMoves.length; move++){
				thisGame[thisPiece.legalMoves[move]].selectable = false;
			}
			for (var cap = 0; cap < thisPiece.legalCaptures.length; cap++){
				thisGame[thisPiece.legalCaptures[cap]].capturable = false;
			}
		},
		commitMove: function(row, col){
			thisPiece = this.piece;
			thisGame = this.gameInstance;
			posBeforeMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posBeforeMove].occupiedBy = null;
			thisPiece.rowPos = row;
			thisPiece.colPos = col;
			posAfterMove = (thisPiece.rowPos * 8) + thisPiece.colPos;
			thisGame[posAfterMove].occupiedBy = thisPiece;
			thisPiece.hasMoved = true;
			this.dehighlightLegalMoves();
			eventBus.$emit('committedMove', {piece:thisPiece, before:posBeforeMove, after:posAfterMove});
		}
	}
})

Vue.component('player-instance', {
	props: ['player','playerInstances'],
	template: 	`
				<div class='player-container'>
					<div>{{player.id}}</div>
					<div class='captured-pieces'></div>
				</div>
				`,
	methods: {

	},
	created: function(){
		//this.generatePlayer();
	}
})

Vue.component('game-instance', {
	props: ['colLabels','rowLabels','tileColors'],
	template: 	`
				<div class='game-environment-container'>
					<col-labels v-bind:colLabels='colLabels' class='col-label-top'></col-labels>
					<col-labels v-bind:colLabels='colLabels' class='col-label-bot'></col-labels>
					<row-labels v-bind:rowLabels='rowLabels' class='row-label-left'></row-labels>
					<row-labels v-bind:rowLabels='rowLabels' class='row-label-right'></row-labels>
					<player-instance v-for='player in playerInstances' v-bind:id='"player"+player.id' v-bind:key='player.id' v-bind:player='player' v-bind:playerInstances='playerInstances'></player-instance>
					<chess-board v-on:commitMove='commitMove' v-on:pieceSelected='pieceSelected' v-bind:gameInstance='gameInstance' v-bind:playerInstances='playerInstances'></chess-board>
				</div>
				`,
	data: function(){
		return {
			gameInstance: [],
			playerInstances: [],
			selectedPiece: null
		}
	},
	methods: {
		generateGameBoard: function(colLabels, rowLabels, tileColors, gameInstance){	
			var tileColor;
			for (var r = 0; r < rowLabels.length; r++){
				for (var c = 0; c < colLabels.length; c++){
					if (r%2 == 0){
						if (c%2 == 0){
							tileColor = 0;
						}else{
							tileColor = 1;
						}
					}else{
						if (c%2 == 0){
							tileColor = 1;
						}else{
							tileColor = 0;
						}
					}
					var tileInstance = 	{
										selectable: false,
										capturable: false,
										row: r,
										col: c,
										id: this.colLabels[c]+String(this.rowLabels[r]),
										color: tileColors[tileColor],
										occupiedBy: null
										};
					gameInstance.push(tileInstance);
				}
			}
		},
		generateStandardGame: function(){
			var player1ID = 0;
			var player2ID = 1;
			var player1 = 	{
							id: player1ID,
							activePieces:[
							this.generatePiece(player1ID,"rook",0,0),this.generatePiece(player1ID,"knight",0,1),this.generatePiece(player1ID,"bishop",0,2),this.generatePiece(player1ID,"king",0,3),this.generatePiece(player1ID,"queen",0,4),this.generatePiece(player1ID,"bishop",0,5),this.generatePiece(player1ID,"knight",0,6),this.generatePiece(player1ID,"rook",0,7),
							this.generatePiece(player1ID,"pawn",1,0),this.generatePiece(player1ID,"pawn",1,1),this.generatePiece(player1ID,"pawn",1,2),this.generatePiece(player1ID,"pawn",1,3),this.generatePiece(player1ID,"pawn",1,4),this.generatePiece(player1ID,"pawn",1,5),this.generatePiece(player1ID,"pawn",1,6),this.generatePiece(player1ID,"pawn",1,7)						]
							};
			this.playerInstances.push(player1);
			var player2 = 	{
							id: player2ID,
							activePieces: [
							this.generatePiece(player2ID,"pawn",6,0),this.generatePiece(player2ID,"pawn",6,1),this.generatePiece(player2ID,"pawn",6,2),this.generatePiece(player2ID,"pawn",6,3),this.generatePiece(player2ID,"pawn",6,4),this.generatePiece(player2ID,"pawn",6,5),this.generatePiece(player2ID,"pawn",6,6),this.generatePiece(player2ID,"pawn",6,7),
							this.generatePiece(player2ID,"rook",7,0),this.generatePiece(player2ID,"knight",7,1),this.generatePiece(player2ID,"bishop",7,2),this.generatePiece(player2ID,"king",7,3),this.generatePiece(player2ID,"queen",7,4),this.generatePiece(player2ID,"bishop",7,5),this.generatePiece(player2ID,"knight",7,6),this.generatePiece(player2ID,"rook",7,7)
							]
							};
			this.playerInstances.push(player2);
		},
		generatePiece: function(playerId, type, row, col){
			var pieceColors = ['white','black'];
			var piece = {
							playerId: playerId,
							pieceId: type+"|"+row+"|"+col+"|"+playerId,
							pieceColor: pieceColors[playerId],
							type: type,
							hasMoved: false,
							legalMoves: [],
							legalCaptures: [],
							moveList: [],
							rowPos: row,
							colPos: col
						}
			return piece;
		},
		loadPiecesToTilesStandard: function(playerInstances, gameInstance){
			for (var player=0; player<playerInstances.length;player++){
				for (var piece=0; piece<playerInstances[player].activePieces.length;piece++){
					var pieceRow = playerInstances[player].activePieces[piece].rowPos;
					var pieceCol = playerInstances[player].activePieces[piece].colPos;
					var tilePos = (8*pieceRow)+pieceCol;
					gameInstance[tilePos].occupiedBy = playerInstances[player].activePieces[piece];
				}
			}
		},
		pieceSelected: function(piece){
			if (this.selectedPiece == null){
				this.selectedPiece = piece;
				this.selectedPiece.highlightLegalMoves();
			}else{
				if (this.selectedPiece != piece){
					this.selectedPiece.dehighlightLegalMoves();
					this.selectedPiece = piece;
					this.selectedPiece.highlightLegalMoves();
				}else{
					this.selectedPiece.dehighlightLegalMoves();
					this.selectedPiece = null;
				}
			}
			
		},
		commitMove: function(row, col){
			this.selectedPiece.commitMove(row, col);
		}
	},
	created: function(){
		this.generateGameBoard(this.colLabels, this.rowLabels, this.tileColors, this.gameInstance);
		this.generateStandardGame();
		this.loadPiecesToTilesStandard(this.playerInstances, this.gameInstance);
	}
})

Vue.component('chess-board', {
	props: ['gameInstance','playerInstances'],
	template: 	`
				<div class='chess-board'>
					<board-tile v-for='gameTile in gameInstance' v-bind:key='gameTile.id' v-bind:gameTile='gameTile' v-bind:gameInstance='gameInstance'></board-tile>
				</div>
				`,
})


Vue.component('board-tile', {
	props: ['gameTile','gameInstance'],
	template: 	`
				<div class='board-tile' v-bind:id='gameTile.id' v-on:click='isLegal' v-bind:style="'background-color: '+gameTile.color">
					<div class='selectable' v-if='gameTile.selectable'></div>
					<div class='capturable' v-if='gameTile.capturable'></div>
					<chess-piece v-if='gameTile.occupiedBy != null' v-bind:gameTile='gameTile' v-bind:gameInstance='gameInstance'></chess-piece>
				</div>
				`,
	methods: {
		isLegal: function(){
			//if (this.gameTile.selectable && this.gameTile.occupiedBy == null){
			if (this.gameTile.selectable || this.gameTile.capturable){
				this.$parent.$emit('commitMove',this.gameTile.row, this.gameTile.col);
				console.log('commitMove');
			}else{
				console.log("nope");
			}
		}
	}
})

Vue.component('row-labels', {
	props: ['rowLabels'],
	template: 	`
				<div class='row-labels'>
					<div v-for='rowLabel in rowLabels' class='row-label'>{{rowLabel}}</div>
				</div>
				`,
})

Vue.component('col-labels', {
	props: ['colLabels'],
	template: 	`
				<div class='col-labels'>
					<div v-for='colLabel in colLabels' class='col-label'>{{colLabel}}</div>
				</div>
				`,
})

Vue.component('move-list', {
	template: 	`
					<div class='global-move-list'>
						<div>Move List</div>
						<move v-for='move in globalMoveList' v-bind:move='move'></move>
					</div>
				`,
	data: function(){
		return {
			globalMoveList: []
		}
	},
	methods: {
		addMove: function(payload){
			var move = 	{
							piece: payload.piece,
							from: payload.before,
							to: payload.after
						}
			console.log(payload.piece);
			this.globalMoveList.push(move);
		}
	},
	created: function(){
		eventBus.$on('committedMove', payload => this.addMove(payload));
	},
	beforeDestroy: function(){
		eventBus.$off('committedMove');
	}
})

Vue.component('move',{
	props: ['move'],
	template: 	`
					<div class='move'>
						{{this.move.piece.pieceId}}*{{this.move.from}}-->{{this.move.to}}
					</div>
				`
})

const eventBus = new Vue({}) //global event bus

new Vue({
	el: '#vue-app',
	template: 	`
				<div class='general-container'>
					<div class='title-bar'>Vue Chess</div>
					<move-list></move-list>
					<game-instance v-bind:colLabels='colLabels' v-bind:rowLabels='rowLabels' v-bind:tileColors='tileColors'></game-instance>	
				</div>
				`,
	data: {
		rowLabels: [8,7,6,5,4,3,2,1],
		colLabels: ['A','B','C','D','E','F','G','H'],
		tileColors: ['#696969','white'],
	}
})